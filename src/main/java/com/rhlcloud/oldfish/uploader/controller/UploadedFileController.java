package com.rhlcloud.oldfish.uploader.controller;

import com.rhlcloud.oldfish.uploader.entity.UploadedFile;
import com.rhlcloud.oldfish.uploader.repository.UploadedFileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
public class UploadedFileController {
    @Autowired
    UploadedFileRepository uploadedFileRepository;

    private final String directory = "file_system_storage";

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public List<UploadedFile> index() {
        return uploadedFileRepository.findAll();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<UploadedFile> show(@PathVariable int id) {
        Optional<UploadedFile> uploadedFile = uploadedFileRepository.findById(id);
        if (uploadedFile.isPresent()) {
            return new ResponseEntity<UploadedFile>(uploadedFile.get(), HttpStatus.OK);
        } else{
            return new ResponseEntity((HttpStatus.NOT_FOUND));
        }
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public ResponseEntity<UploadedFile> create(@RequestParam MultipartFile file) {
        String filepath;
        try {
            String filename = file.getOriginalFilename();
            filename = UUID.randomUUID() + "_" + filename;
            filepath = Paths.get(directory, filename).toString();

            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(new File(filepath)));
            bufferedOutputStream.write(file.getBytes());
            bufferedOutputStream.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }

        UploadedFile uploadedFile = new UploadedFile();
        uploadedFile.setPath(filepath);
        uploadedFileRepository.saveAndFlush(uploadedFile);
        return  new ResponseEntity<UploadedFile>(uploadedFile, HttpStatus.OK);
    }
}
