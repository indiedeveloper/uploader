package com.rhlcloud.oldfish.uploader.repository;

import com.rhlcloud.oldfish.uploader.entity.UploadedFile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UploadedFileRepository extends JpaRepository<UploadedFile, Integer> {
    Optional<UploadedFile> findById(int id);
}
