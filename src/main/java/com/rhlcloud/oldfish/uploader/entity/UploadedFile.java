package com.rhlcloud.oldfish.uploader.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class UploadedFile {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private int id;
    private String path;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UploadedFile that = (UploadedFile) o;

        if (id != that.id) return false;
        return path.equals(that.path);

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + path.hashCode();
        return result;
    }
}
