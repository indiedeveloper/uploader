package com.rhlcloud.oldfish.uploader;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestfulFileUploaderApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestfulFileUploaderApplication.class, args);
	}
}
